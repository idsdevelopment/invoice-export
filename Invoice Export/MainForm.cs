﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using IDS_InvoiceExport.Errors;
using IDS_InvoiceExport.Export;
using IDS_InvoiceExport.IDS;
using Invoice_Export.Properties;

namespace IDS_InvoiceExport
{
	public partial class MainForm : Form
	{
		private const string VERSION = " Vsn 1.7";

		//		const string ERROR_PATH = @"Error\";

		private bool AllowClose;

		public MainForm() { InitializeComponent(); }


		private void LoadSettings()
		{
			var D = Settings.Default;
			D.Reload();

			CarrierId.Text = D.CarrierId;
			AccountId.Text = D.AccountId;
			UserId.Text = D.UserId;
			IdsPassword.Text = D.IdsPassword;
			LastInvoiceNumber.Value = D.IDS_InvoiceNumber;
			ManualImportPath.Text = D.ManualImportPath;
			ErrorPath.Text = D.ErrorPath;
			CsvExportPath.Text = D.CsvExportPath;
			DebugMode.Checked = D.DebugMode;
			NextPollTime.Value = D.NextPolTime;
			NextPollInterval.Value = D.NextImportInterval;
			AccountPrefix.Text = D.AccountPrefix;
			InvoicePostingDate.Value = DateTime.Today.AddDays( -61 );
			PrimaryServer.Checked = D.PrimaryServer;
			SecondaryServer.Checked = D.SecondaryServer;
		}


		private void SaveSettings()
		{
			var D = Settings.Default;

			D.CarrierId = CarrierId.Text;
			D.AccountId = AccountId.Text;
			D.UserId = UserId.Text;
			D.IdsPassword = IdsPassword.Text;
			D.IDS_InvoiceNumber = LastInvoiceNumber.Value;
			D.ManualImportPath = ManualImportPath.Text;
			D.ErrorPath = ErrorPath.Text;
			D.CsvExportPath = CsvExportPath.Text;
			D.DebugMode = DebugMode.Checked;

			D.NextPolTime = NextPollTime.Value;
			D.NextImportInterval = NextPollInterval.Value;
			D.AccountPrefix = AccountPrefix.Text;
			D.PrimaryServer = PrimaryServer.Checked;
			D.SecondaryServer = SecondaryServer.Checked;
			D.Save();
		}

		private void MainForm_Load( object sender, EventArgs e )
		{
			LoadSettings();

			closeSettingsToolStripMenuItem_Click( sender, e );
		}

		private void exitToolStripMenuItem_Click( object sender, EventArgs e )
		{
			AllowClose = true;
			Close();
		}

		private void closeSettingsToolStripMenuItem_Click( object sender, EventArgs e )
		{
			CloseSettings.Enabled = false;
			var Pages = MainTabControl.TabPages;
			Pages.Remove( ImportSettingsTabPage );
			Pages.Remove( StatusTabPage );
			Pages.Add( StatusTabPage );

			SaveSettings();
		}

		private void ImportSettingsMenuItem_Click( object sender, EventArgs e )
		{
			CloseSettings.Enabled = true;
			var Pages = MainTabControl.TabPages;
			Pages.Remove( StatusTabPage );
			Pages.Add( ImportSettingsTabPage );
		}


		// Needed because of Bug in radio buttons
		private void MainForm_Shown( object sender, EventArgs e )
		{
			// ReSharper disable once ConvertToConstant.Local
			var Version = VERSION;
#if DEBUG
			Version += " (TEST VERSION)";
#endif
			Text += Version;
		}

		private void MainForm_FormClosed( object sender, FormClosedEventArgs e ) { SaveSettings(); }

		private bool ShowBalloon( string Txt )
		{
			var Retval = TrayIcon.Visible;
			if( Retval )
			{
				TrayIcon.BalloonTipText = Txt;
				TrayIcon.ShowBalloonTip( 10000 );
			}
			return Retval;
		}

		private void ShowImportErrors( bool isOk, string errorText )
		{
			if( isOk )
			{
				const string Txt = "Exported with no errors";

				if( !ShowBalloon( Txt ) )
					MessageBox.Show( Txt, "Export Ok", MessageBoxButtons.OK, MessageBoxIcon.Asterisk );
			}
			else
				new ErrorsForm().Show( errorText );
		}

		private void ManualImportBtn_Click( object sender, EventArgs e )
		{
			ManualImportBtn.Enabled = false;
			Application.DoEvents();
			try
			{
				var Dir = ManualImportPath.Text;
				Directory.CreateDirectory( Dir );
				OpenImportFileDialog.InitialDirectory = Dir;
				if( OpenImportFileDialog.ShowDialog() == DialogResult.OK )
				{
					try
					{
						using( var Csv = new FileStream( OpenImportFileDialog.FileName, FileMode.Open, FileAccess.Read ) )
						{
							try
							{
								var Export = new IdsInvoiceExport( CsvExportPath.Text, ErrorPath.Text );
								int HighestInvoiceNumber;
								string ErrorText;
								var IsOk = Export.ExportCsv( Csv, DebugMode.Checked, CsvExportPath.Text, ErrorPath.Text, out HighestInvoiceNumber, out ErrorText );

								LastInvoiceNumber.Value = Math.Max( LastInvoiceNumber.Value, HighestInvoiceNumber );
								SaveSettings();

								ShowImportErrors( IsOk, ErrorText );
							}
							catch( Exception E )
							{
								MessageBox.Show( E.Message, "Error", MessageBoxButtons.OK );
							}
						}
					}
					catch
					{
						MessageBox.Show( "Cannot open import file", "Error", MessageBoxButtons.OK );
					}
				}
			}
			catch( Exception E )
			{
				MessageBox.Show( E.Message, "Error", MessageBoxButtons.OK );
			}
		}

		private void button4_Click( object sender, EventArgs e )
		{
			FolderBrowserDialog.SelectedPath = ManualImportPath.Text;
			if( FolderBrowserDialog.ShowDialog() == DialogResult.OK )
				ManualImportPath.Text = FolderBrowserDialog.SelectedPath;
		}

		private string CustomerPlaceHolder() { return "CustomCSVExportPlaceHolder" + AccountId.Text.Trim(); }

		private void GetInvoicesBtn_Click( object sender, EventArgs e )
		{
			GetInvoicesBtn.Enabled = false;
			var Processing = true;

			var Export = new IdsInvoiceExport( CsvExportPath.Text, ErrorPath.Text );
			var HighestInvoiceNumber = -1;
			var ErrorText = "";
			var IsOk = false;
			var NothingToImport = false;
			var Prefix = AccountPrefix.Text.TrimStart();
			var EndingInvoice = (int)EndingInvoiceNumber.Value;
			var LastInvoice = (int)LastInvoiceNumber.Value;
			var PostingDate = InvoicePostingDate.Value;

			var Server = PrimaryServer.Checked ? Communications.SERVER.PRIMARY : Communications.SERVER.SECONDARY;

			try
			{
				Task.Factory.StartNew( () =>
				{
					try
					{
						var Coms = new Communications( Server, CarrierId.Text.Trim(),
													   AccountId.Text.Trim(),
													   UserId.Text.Trim(),
													   IdsPassword.Text,
													   LastInvoice,
													   CustomerPlaceHolder()
							);

						var Debug = DebugMode.Checked;
						var ErrP = ErrorPath.Text;
						var ExportPath = CsvExportPath.Text;

						Coms.GetCsv( Csv =>
						{
							if( Csv.Length > 0 )
								IsOk = Export.ExportCsv( IdsReMap.ReMapCsv( Csv, Debug, ErrP, EndingInvoice, Prefix, PostingDate ), Debug, ExportPath, ErrP, out HighestInvoiceNumber, out ErrorText );
							else
								NothingToImport = true;

							Processing = false;
						}, errorText =>
						{
							Processing = false;
							IsOk = false;
							ErrorText = errorText;
						}
							);
					}
					catch( Exception E )
					{
						ErrorText = E.Message;
						IsOk = Processing = false;
					}
				} );
			}
			catch( Exception E )
			{
				IsOk = Processing = false;
				ErrorText = E.Message;
			}
			finally
			{
				while( Processing )
				{
					Thread.Sleep( 100 );
					Application.DoEvents();
				}

				LastInvoiceNumber.Value = Math.Max( LastInvoiceNumber.Value, HighestInvoiceNumber );
				SaveSettings();

				const string Txt = "No invoices available for export";

				if( NothingToImport && !ShowBalloon( Txt ) )
					MessageBox.Show( Txt, "Nothing to export", MessageBoxButtons.OK, MessageBoxIcon.Information );
				else
					ShowImportErrors( IsOk, ErrorText );

				GetInvoicesBtn.Enabled = true;
			}
		}


		private void button3_Click( object sender, EventArgs e )
		{
			FolderBrowserDialog.SelectedPath = ErrorPath.Text;
			if( FolderBrowserDialog.ShowDialog() == DialogResult.OK )
				ErrorPath.Text = FolderBrowserDialog.SelectedPath;
		}

		private void button5_Click( object sender, EventArgs e )
		{
			FolderBrowserDialog.SelectedPath = CsvExportPath.Text;
			if( FolderBrowserDialog.ShowDialog() == DialogResult.OK )
				CsvExportPath.Text = FolderBrowserDialog.SelectedPath;
		}

		/*
		private void NumericKeyPress( object sender, KeyPressEventArgs e )
		{
			if( !char.IsDigit( e.KeyChar ) && e.KeyChar != (char)Keys.Back ) 
			{
				SystemSounds.Beep.Play();
				e.Handled = true;
			}
		}
*/

		private void MainForm_Resize( object sender, EventArgs e )
		{
			switch( WindowState )
			{
			case FormWindowState.Minimized:
				TrayIcon.Visible = true;
				ShowInTaskbar = false;
				PollTimer.Enabled = true;
				break;

			default:
				TrayIcon.Visible = false;
				ShowInTaskbar = true;
				PollTimer.Enabled = false;
				break;
			}
		}

		private void showToolStripMenuItem_Click( object sender, EventArgs e )
		{
			ShowInTaskbar = true;
			TrayIcon.Visible = false;
			WindowState = FormWindowState.Normal; // Leave Last, Stops Flicker
		}

		private void PollTimer_Tick( object sender, EventArgs e )
		{
			MethodInvoker UIMethod = delegate
			{
				PollTimer.Enabled = false;
				try
				{
					var NextPol = NextPollTime.Value;
					if( NextPol < DateTime.Now )
					{
						NextPollTime.Value = NextPol.AddHours( (double)NextPollInterval.Value );
						SaveSettings();
						ShowBalloon( "Fetching Invoices." );
						GetInvoicesBtn_Click( sender, e );
					}
				}
				finally
				{
					PollTimer.Enabled = true;
				}
			};

			Invoke( UIMethod );
		}

		private void MainForm_FormClosing( object sender, FormClosingEventArgs e )
		{
			if( !AllowClose )
			{
				if( MessageBox.Show( "Minimise to the icon tray?", "Hide", MessageBoxButtons.YesNo, MessageBoxIcon.Question ) == DialogResult.Yes )
				{
					e.Cancel = true;
					WindowState = FormWindowState.Minimized;
				}
			}
		}

		private void TrayIcon_Click( object sender, EventArgs e ) { WindowState = FormWindowState.Normal; }
	}
}