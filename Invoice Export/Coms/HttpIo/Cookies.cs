﻿using System.Net;

namespace IDS_InvoiceExport.IDS.Http
{
	public partial class HttpIo : Disposable
	{
		public static void InitCookie()
		{
			lock( CookieLock )
			{
				GlobalCookies = new CookieContainer();
			}
		}
	}
}