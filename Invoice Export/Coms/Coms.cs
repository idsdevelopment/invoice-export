﻿using System;
using System.IO;
using IDS_InvoiceExport.IDS.Http;

namespace IDS_InvoiceExport.IDS
{
	internal class Communications
	{
		private readonly string ComsLink;

		private const string PrimaryServerLink = "http://users.internetdispatcher.org:8080/ids/reports?carrierId=%CID%&accountId=%ACID%&userId=%UID%&p=%PASS%&report=%REPORT%&output=99&invoiceId=%INV_ID%&targetDriverId=&targetStaffId=&targetAccountId=&from=&to=&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true";
		private const string SecondaryServerLink = "http://reports.internetdispatcher.org:8080/ids/reports?carrierId=%CID%&accountId=%ACID%&userId=%UID%&p=%PASS%&report=%REPORT%&output=99&invoiceId=%INV_ID%&targetDriverId=&targetStaffId=&targetAccountId=&from=&to=&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true";

		public enum SERVER
		{
			PRIMARY,
			SECONDARY
		};

		internal Communications( SERVER server, string carrierId, string accountId, string userId, string password, int lastInvoiceNumber, string provider )
		{
			var Link = ( server == SERVER.PRIMARY ? PrimaryServerLink : SecondaryServerLink );
			var StartingInvoice = lastInvoiceNumber.ToString();

			ComsLink = Link.Replace( "%CID%", carrierId )
						   .Replace( "%ACID%", accountId )
						   .Replace( "%UID%", userId )
						   .Replace( "%PASS%", password )
						   .Replace( "%INV_ID%", StartingInvoice )
						   .Replace( "%REPORT%", provider );
		}

		internal void GetCsv( Action<Stream> onSuccess, Action<string> onFail )
		{
			var Csv = "";
			var ReplyError = false;
			var Reason = "";

			HttpIo.WebIo.Get( new HttpIo.RequestEntry(	ComsLink, Reply =>
														{
															Csv = Reply.Content;
														}, Reply =>
														{
															ReplyError = true;
															Reason = Reply.Content.Trim();
														},
														true
													 )
							);

			if( ReplyError || Csv.Contains( Reason = "Authentication Error" ) )
				onFail( Reason );
			else
				onSuccess( new MemoryStream( System.Text.Encoding.UTF8.GetBytes( Csv ) ) );
		}
	}

}
