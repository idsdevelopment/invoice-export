﻿using System;

namespace IDS_InvoiceExport
{
	internal static class Globals
	{
		internal const string ERROR_EXTENSION = ".error.log",
							  CSV_EXTENSION = ".csv";
	}
}
